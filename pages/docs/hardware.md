# Measuring techniques research

**Mass air flow and Differential pressure sensor**

Mass airflow sensors should only be used when the gas flowing through them is known to be free of contaminants.

Mass airflow sensors are only to be used when there is a uncontaminated flow of air that is able to pass through a filter. 
They are primarily used in vehicles with combustible fuel.

Differential Pressure sesnsor utilizes the effect of gasses and liquids expanding and contracting from changes in either pressure or temperature. This is very useful if you need a reading where there is no airflow or movement of gasses.

The main difference can be seen in the picture, as the mass airflow sensor is much more effective at low values.
The slope of the pressure sensor should remain consistent compared to the mass airflow. This means that Mass airflow sensors have a better resolution at lower values.

![Image](1Capture.JPG)

**Thermocouples**

*What is it?* - a thermocouple consists of two dissimilar metals.  These two different metals are welded together at one end creating junction between them.  It is at the point where the temperature can actually be measured

*How does it work?*- When the two conjoined metal types experience any kind of temperature change, there is a very specific voltage that is created.  Based off the amount of voltage that is created, you can determine the temperature very accurately

*Why/When do you use a Thermocouple ?**

* They are alot cheapter than RTD's, up to 2-3 times
* Depending on the type of thermocouple, they somtimes can have highter temperature meassures than RTD's
* In general they are considered more durable than RTD's or other meassures devices

*Documentaion links*

* https://www.youtube.com/watch?v=abC-7OIwgTU&ab_channel=RSPSupply

**Capacitive Humidity Sensors**

Humidity Sensors based on capacitive effect or simply Capacitive Humidity Sensors are one of the basic types of Humidity Sensors available.

They are often used in applications where factors like cost, rigidity and size are s of concern. In Capacitive Relative Humidity (RH) Sensors, the electrical permittivity of the dielectric material changes with change in humidity.

**Resistive Humidity Sensors (Electrical Conductivity Sensors)**

Resistive Humidity Sensors are another important type of Humidity Sensors that measure the resistance (impedance) or electrical conductivity. The principle behind resistive humidity sensors is the fact that the conductivity in non – metallic conductors is dependent on their water content.

Working of Resistive Humidity Sensors
The Resistive Humidity Sensor is usually made up of materials with relatively low resistivity and this resistivity changes significantly with changes in humidity. The relationship between resistance and humidity is inverse exponential. The low resistivity material is deposited on top of two electrodes.

The electrodes are placed in interdigitized pattern to increase the contact area. The resistivity between the electrodes changes when the top layer absorbs water and this change can be measured with the help of a simple electric circuit.

*Documentaion links*

* https://www.youtube.com/watch?v=abC-7OIwgTU&ab_channel=RSPSupply


# Sensor requirement draft

* 3 sensors. Measuring temperature, humidty, airflow, Co2 and TVOC
* Node red dashboard, showing graph over time with sensors(OME needs)
* Extra equipment for airflow sensor (CO2 tubes, vacum pipe)

# List of possible sensors

| sensor name   | cost  | pros  | cons  | link  |
|---|---|---|---|---|
| DHT11 | kr. 33.39 | cheap, two sensors in one | bad accuracity, but sufficient | https://dk.farnell.com/en-DK/dfrobot/dfr0067/temperature-humidity-sensor-arduino/dp/2946103?st=dht11|
| Sensirion SDP810-125 PA | kr. 276.62 | can measure accurat in airflow | quite expensiv  | https://dk.farnell.com/en-DK/sensirion/sdp810-125pa/pressure-sensor-digital-125pa/dp/2886665?ost=sdp810-125+pa&cfm=true |
| AMS CCS811 Sensor | kr. 165,42 | can measure airquality for humans| expensive | https://dk.farnell.com/en-DK/digilent/410-386/pmod-aqs-board-air-quality/dp/3003248?st=air%20quality%20sensor |
