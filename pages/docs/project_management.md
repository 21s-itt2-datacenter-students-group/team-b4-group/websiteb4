# Project Management
We used the SCRUM in order to organize the workflow in such a way that we can immediately pick up after last weeks progress. 

Every week we designated new people to the roles of Secretary and SCRUM master, and using the sprint we were able to minimize the amount of time we use for planning and dishing out responsibilities. 

We used the issue board to quickly determine where everybody is in development, and see what tasks are still left from the previous week. 

Once we decide what to do with the remaining tasks we begin handing out new tasks to individuals. While it is allowed and encouraged to work in 
small groups in the team, the task will handed to a specific individual due to how gitlab works.

The fact that we started every day the same way insures consistency, and only using 15 minutes allows us not to waste valuable time.
