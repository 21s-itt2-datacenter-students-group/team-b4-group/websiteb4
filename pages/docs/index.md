# Team B4 Website

## Introduction

In this project we are creating a system that is reading and writing data from sensors. It is designed for being used in a datacenter.
We are focusing on monitoring air conditions, to ensure a healthy enviorement. And with the increasing growth of datacenters, it's also a important factor. The data collected from the sensors, are displayed on a dashboard, for a better overview of air measurements. This data is also collected in a database, so that the owner can look back on all data measured.

## System purpose

The purpose of the system, is using sensors to measure air conditions to ensure a healthy 
environment for humans working in the datacenter industry.

## System overview

![System overview](diagrama.png)

## Screenshots from dash board

![Temp screenshot](temp_example.jpg)
