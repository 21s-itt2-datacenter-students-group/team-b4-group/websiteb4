# Gitlab group

* https://gitlab.com/21s-itt2-datacenter-students-group/team-b4

* https://gitlab.com/21s-itt2-datacenter-students-group/team-b4-group

# Course material

* https://eal-itt.gitlab.io/21s-itt2-project/

* https://eal-itt.gitlab.io/21s-itt2-project/exercises/

# Articles

 * https://www.scrum.org/resources/what-is-a-scrum-master

 * https://www.iotforall.com/mqtt-security-three-basic-concepts

 * https://blog.paessler.com/why-mqtt-is-everywhere-and-the-security-issues-it-faces

 * https://www.youtube.com/watch?v=1FoCbbbcYT8

 * https://www.fictiv.com/articles/5-essential-tips-for-user-testing-your-hardware-project

 * https://www.agilealliance.org/glossary/mvp#q=~(infinite~false~filters~(tags~(~'mvp))~searchTerm~'~sort~false~sortDirection~'asc~page~1)

 * https://www.mongodb.com/nosql-explained

 * https://nodered.org/docs/getting-started/azure

 * https://www.productplan.com/glossary/minimum-viable-product/

 * https://www.youtube.com/watch?v=E4ex0fejo8w

 * https://www.youtube.com/watch?v=X8ustpkAJ-U

 * https://mntolia.com/mqtt-last-will-testament-explained-with-examples/

 * https://userpeek.com/blog/user-experience-testing/

 * https://www.hivemq.com/mqtt-security-fundamentals/

 * https://www.yukti.io/10-most-important-user-testing-methods/

# Official documentation

* https://github.com/JJSlabbert/Sensirion_SDP810 (sensorian)

* https://reference.digilentinc.com/pmod/pmodaqs/reference-manual (Pmod AQS)

* https://www.youtube.com/watch?v=D1Acq_LUC40 (DHT11)


