# MVP user test
Question | Yes | No | everything is fine | other
:------------ | :------------- | :------------- | :------------- | :-------------
Was The MVP(minimum viable product) video informative | Yes |  |  |
What did you like the most/least about our MVP video |  |  |  | fine video, but the sound was a bit low
Do you have any experience with the Software or Hardware that we are using |  | No |  |
What do you think could be improved | | | everything is fine  |
Is there anything you would like to change about our collaboration  | | | everything is fine  |
Did we meet your expectations | Yes |  |  |  |
Was this survey useful  | Yes |  |  |  |
 

# Unit testing
The unit testing is a good tool to avoid function breaking updatates in gitlab there is an example of our unit testing.
https://gitlab.com/ReFresh20/unit-testing-b4
# System test planning
# Internet
Problem - internet connection error. 

Solution - small python code that checks if its connected to internet.

Results - results will be shown in Rednode.

# Coding
Problem - Coding mistakes or a typo in a code.

Solution - Double checking the code, using debug option.

Results - Once there are no errors the code will be tested with raspberry pi and sensors.
# Hardware
Problem - Sensor malfunction test

Solution - Use python code that comes with the sensor.

Results - The problem with sensor will be identified if there is one.





