## System brainstorm

![alt text for screen readers](Untitled_Diagram_1_.jpg "Brainstorm")

## Use Case

To measure temperature, humidity, airflow, Co2 and TVOC. To ensure better human conditions in data center environment

## Requirements draft

* 3 sensors. Measuring temperature, humidty, airflow, Co2 and TVOC
* Node red dashboard, showing graph over time with sensors(OME needs)
* Extra equipment for airflow sensor (CO2 tubes, vacum pipe)
